#!/usr/bin/env python

'''
Takes all pdfs from the current directory and puts them into one single file,
ordered by modification date. pdftk has to be installed.
'''

import os, glob, time, subprocess


def main():
	'''
	Does all the work
	'''
	# Get all pdf-files in the cwd an sort them by modification date
	files = sorted([(os.stat(filename).st_mtime, filename) for filename in glob.glob("*.pdf")])

	# Extract the filenames
	files = [filename for (mtime, filename) in files]

	# Build the command to concat the files
	command = ["pdftk"]
	command.extend(files)
	command.extend(["cat", "output", "output.pdf"])

	# Execute the command
	subprocess.call(command)


# If this is executed as program call the main function
if (__name__ == "__main__"):
	main()
